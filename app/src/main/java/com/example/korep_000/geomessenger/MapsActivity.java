package com.example.korep_000.geomessenger;

import android.Manifest;
import android.app.DownloadManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private boolean Flag_text = false;
    private double Longitude;
    private double Latitude;
    private double loc_longi;
    private double loc_lat;
    private double longi = 37.752399; // place with message
    private double lat = 55.756060;   // place with message
    private String StrCatName = "";
    Handler h;

    final int STATUS_MSG = 1; // new message
    final int STATUS_NONE = 0;

    private LocationManager locationManager;
    private android.location.LocationListener myLocationListener;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
       SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        Uri gmmIntentUri = Uri.parse("");
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        if (mapIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(mapIntent);
        }
        RelativeLayout rl = (RelativeLayout) findViewById(R.id.rl);

        Button btn = new Button(getApplicationContext());
        RelativeLayout.LayoutParams lp_btn = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT, // Width of TextView
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        lp_btn.setMargins(10, 10, 0, 10);

        btn.setLayoutParams(lp_btn);
        rl.addView(btn);
        btn.setText("Menu");
        int color_btn = Color.parseColor("#FFFFF0");
        btn.setTextColor(color_btn);
        color_btn = Color.parseColor("#5F9EA0");
        btn.setBackgroundColor(color_btn);
        btn.setTextSize(20);

        final EditText tv = new EditText(getApplicationContext());
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT, // Width of TextView
                RelativeLayout.LayoutParams.WRAP_CONTENT); // Height of TextView
        lp.setMargins(10 + btn.getWidth(), 10, 10, 10); //left, top, right, bottom
        // Apply the layout parameters to TextView widget
        lp.addRule(RelativeLayout.RIGHT_OF, btn.getId());
        tv.setLayoutParams(lp);
        int color = Color.parseColor("#FFFFF0"); //white
        tv.setBackgroundColor(color);
        color = Color.parseColor("#000000"); //black
        tv.setTextColor(color);
        color = Color.parseColor("#A9A9A9");
        tv.setTextSize(20);
        tv.setGravity(Gravity.CENTER);
        rl.addView(tv);

        tv.setOnKeyListener(new View.OnKeyListener()
                            {
                                public boolean onKey(View v, int keyCode, KeyEvent event)
                                {
                                    if(event.getAction() == KeyEvent.ACTION_DOWN &&
                                            (keyCode == KeyEvent.KEYCODE_ENTER))
                                    {
                                        // сохраняем текст, введенный до нажатия Enter в переменную
                                        StrCatName = tv.getText().toString();
                                        coordinates(StrCatName);
                                        Flag_text = true;
                                        onMapReady(mMap);
                                        return true;
                                    }
                                    return false;
                                }
                            }
        );
        View.OnClickListener oclBtnOk = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MapsActivity.this.openOptionsMenu();
            }
        };

        btn.setOnClickListener(oclBtnOk);
        h = new Handler() {
            public void handleMessage(android.os.Message msg) {
                switch (msg.what) {
                    case STATUS_MSG:
                        Toast toast = Toast.makeText(getApplicationContext(),
                                "New message", Toast.LENGTH_SHORT);
                        toast.show();
                        break;
                    case STATUS_NONE:
                        break;
                }
            };
        };
        h.sendEmptyMessage(STATUS_NONE);
        check_location();
       /* if ( ContextCompat.checkSelfPermission( this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_DENIED ) {
            Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            onLocationChanged(location);
        }
        check_location();
*/
    }
  /*  public void onLocationChanged(Location location) {
        if(location!=null) {
            loc_lat = location.getLatitude();
            loc_longi = location.getLongitude();
            Toast toast = Toast.makeText(getApplicationContext(),
                    "LOCATION", Toast.LENGTH_SHORT);
            toast.show();
        }
        return;
    }*/
    void check_location() {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                while(true) {
                    if ((longi - Longitude)*(longi - Longitude)
                            + (lat - Latitude)*(lat - Latitude) <= 0.0036) {
                        h.sendEmptyMessage(STATUS_MSG);
                        break;
                    }
                }
            }
        });
        t.start();
    }

    public void coordinates(String adress) {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocationName(adress, 1);
            Address address = addresses.get(0);
            Longitude = address.getLongitude();
            Latitude = address.getLatitude();
            /*Toast toast = Toast.makeText(getApplicationContext(),
                        Double.toString(Longitude) + " " + Double.toString(Latitude), Toast.LENGTH_SHORT);
            toast.show();*/
            StrCatName = "";
        } catch (IOException e){
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.settings) {
            GetUrl ex = new GetUrl();
            try {
                ex.run();
            } catch(Exception e){

            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (Flag_text) {
            Toast toast1 = Toast.makeText(getApplicationContext(),
                    Double.toString(Latitude) + " " + Double.toString(Longitude), Toast.LENGTH_SHORT);
            toast1.show();
            // Add a marker in Sydney and move the camera
            LatLng sydney = new LatLng(Latitude, Longitude);
            mMap.addMarker(new MarkerOptions().position(sydney).title("My location"));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
            Flag_text = false;
        }
        /*// Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));*/
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_DENIED) {
            mMap.setMyLocationEnabled(true);
        } else {

        }
    }
}

