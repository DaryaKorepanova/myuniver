package com.example.korep_000.geomessenger;

import android.util.Log;
import android.widget.Toast;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by korep_000 on 02.06.2017.
 */

public class GetUrl {
    Request request;
    Response response;
    private final OkHttpClient client = new OkHttpClient();

    public void run() throws Exception {
        Request request = new Request.Builder()
                .url("http://publicobject.com/helloworld.txt")
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

                /*Headers responseHeaders = response.headers();
                for (int i = 0, size = responseHeaders.size(); i < size; i++) {
                    System.out.println(responseHeaders.name(i) + ": " + responseHeaders.value(i));
                }*/

                //System.out.println(response.body().string());
            }
        });
    }
/*
    public static void main(String[] args) throws IOException {
        GetUrl example = new GetUrl();
        String response = example.start("https://raw.github.com/square/okhttp/master/README.md");
        System.out.println(response);
    }*/
}
